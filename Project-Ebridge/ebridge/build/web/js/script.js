var app = angular.module('myApp', []);

app.controller('Ctrl3', function ($scope, $http) {

    $scope.number;

    $scope.people = [];
    $scope.loadStatistic = function () {

        var route = '/ebridge/webresources/statistic?idtest=' + $scope.number;
        $http.get(route)
                .success(function (data) {
                    $scope.people = data;
                });
    };

});

app.controller('AlarmCtrl', function ($scope, $http) {

    $scope.alarmas = [];

    var route = '/ebridge/webresources/alarm/getalarms';
    $http.get(route)
            .success(function (data) {
                $scope.alarmas = data;
            });

});


app.controller('ModalCtrl', function ($scope) {

    $scope.showModal = function () {
        app.element('#myModalShower').trigger('click');
    };

});

app.controller('SendConfigCtrl', function ($scope, $http) {


    $scope.enviar = function () {

        var idPrueba = $scope.sendConfig.idPrueba;
        var tiempo = $scope.sendConfig.tiempo;
        var dispositivo = $scope.sendConfig.dispositivo;
        var sensor = $scope.sendConfig.sensor;
        var gender = $scope.sendConfig.gender;

        var data_json = {"idconfiguracion": 0, "idprueba": idPrueba, "periodo": tiempo, "realizado": false, "sensor": sensor, "device": dispositivo, "estadistica": gender};
        var parameter = JSON.stringify(data_json);
        $http.post('/ebridge/webresources/configuration', parameter).
                success(function (data) {

                    if (data === 'Store on DB') {
                        $scope.success = {display: 'block'};
                        $scope.messageSuccess = "Exito al guardar la configuracion";
                    } else {
                        $scope.danger = {display: 'block'};
                        $scope.messageDanger = "Error al guardar la configuracion";
                    }

                }).
                error(function (data) {
                    $scope.danger = {display: 'block'};
                    $scope.messageDanger = "Error al guardar la configuracion";
                });

    };

    $scope.deleteMessage = function () {
        $scope.success = {display: 'none'};
        $scope.messageSuccess = "";
        $scope.danger = {display: 'none'};
        $scope.messageSDanger = "";
    };

});