/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.dao.StatisticDAO;

import com.ebridge.Entities.Estadistica;
import com.ebridge.Utils.HibernateUtil;
import com.ebridge.dao.Generic.GenericDAOImp;
import java.math.BigDecimal;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Melvin
 */
public class StatisticDAO extends GenericDAOImp<Estadistica, BigDecimal> implements StatisticDAOInt {

    @Override
    public List<Estadistica> findByTest(int idtest) {
        List<Estadistica> list;
        String sql = "from Estadistica where idprueba = :idtest  ORDER BY id_estadistica";
        Query query = HibernateUtil.getSession().createQuery(sql).setParameter("idtest", idtest);
        list = findMany(query);
        return list;
    }

}
