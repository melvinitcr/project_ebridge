/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.dao.StatisticDAO;

import com.ebridge.Entities.Estadistica;
import com.ebridge.dao.Generic.GenericDAO;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Melvin
 */
public interface StatisticDAOInt extends GenericDAO<Estadistica, BigDecimal> {

    public List<Estadistica> findByTest(int idtest);
}
