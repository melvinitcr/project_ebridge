/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.dao.Configuration;

import com.ebridge.Entities.Configuracion;
import com.ebridge.dao.Generic.GenericDAO;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author melvinitcr
 */
public interface ConfigurationDAOInt extends GenericDAO<Configuracion, BigDecimal> {
    
     public List<Configuracion> findBySensor(String Sensor);
}
