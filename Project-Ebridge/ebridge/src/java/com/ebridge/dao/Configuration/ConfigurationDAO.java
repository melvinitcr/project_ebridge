/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.dao.Configuration;

import com.ebridge.Entities.Configuracion;
import com.ebridge.Utils.HibernateUtil;
import com.ebridge.dao.Generic.GenericDAOImp;
import java.math.BigDecimal;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author melvinitcr
 */
public class ConfigurationDAO extends GenericDAOImp<Configuracion, BigDecimal> implements ConfigurationDAOInt {

    @Override
    public List<Configuracion> findBySensor(String device) {
        List<Configuracion> list;
        String sql = "from Configuracion where device = :iddevice and realizado = FALSE";
        Query query = HibernateUtil.getSession().createQuery(sql).setParameter("iddevice", device);
        list = findMany(query);
        return list;
    }

}
