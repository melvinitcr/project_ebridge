/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.dao.Sensortype;

import com.ebridge.dao.Generic.GenericDAO;
import com.ebridge.Entities.Tiposensor;
import java.math.BigDecimal;

/**
 *
 * @author melvinitcr
 */
public interface SensorTypeDAOInt extends GenericDAO<Tiposensor,BigDecimal> {
    
}
