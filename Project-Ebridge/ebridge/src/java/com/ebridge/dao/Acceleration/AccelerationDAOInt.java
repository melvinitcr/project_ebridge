/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.dao.Acceleration;

import com.ebridge.dao.Generic.GenericDAO;
import com.ebridge.Entities.Aceleracion;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author melvinitcr
 */
public interface AccelerationDAOInt extends GenericDAO<Aceleracion, BigDecimal>{
 
      public List<Aceleracion> findByTest(int idtest);
    
}
