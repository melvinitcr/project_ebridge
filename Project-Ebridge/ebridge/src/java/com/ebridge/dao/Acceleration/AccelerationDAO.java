/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.dao.Acceleration;

import com.ebridge.dao.Generic.GenericDAOImp;
import com.ebridge.Entities.Aceleracion;
import com.ebridge.Utils.HibernateUtil;
import java.math.BigDecimal;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author melvinitcr
 */
public class AccelerationDAO extends GenericDAOImp<Aceleracion, BigDecimal> implements AccelerationDAOInt {

    @Override
    public List<Aceleracion> findByTest(int idtest) {
        List<Aceleracion> list;
        String sql = "from Aceleracion where idprueba = :idtest  ORDER BY idaceleracion";
        Query query = HibernateUtil.getSession().createQuery(sql).setParameter("idtest", idtest);
        list = findMany(query);
        return list;
    }

}
