/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.dao.Alarm;

import com.ebridge.Entities.Alarma;
import com.ebridge.Utils.HibernateUtil;
import com.ebridge.dao.Generic.GenericDAOImp;
import java.math.BigDecimal;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author melvinitcr
 */
public class AlarmDAO extends GenericDAOImp<Alarma, BigDecimal> implements AlarmDAOInt {

    @Override
    public List<Alarma> findAlarms() {
        List<Alarma> list;
        String sql = "from Alarma where vista = FALSE";
        Query query = HibernateUtil.getSession().createQuery(sql);
        list = findMany(query);
        return list;
    }

    @Override
    public List<Alarma> findAllAlarms() {
        List<Alarma> list;
        String sql = "from Alarma ORDER BY fecha desc";
        Query query = HibernateUtil.getSession().createQuery(sql);
        list = findMany(query);
        return list;
    }

}
