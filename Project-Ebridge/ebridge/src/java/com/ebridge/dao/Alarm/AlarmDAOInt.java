/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.dao.Alarm;

import com.ebridge.Entities.Alarma;
import com.ebridge.dao.Generic.GenericDAO;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author melvinitcr
 */
public interface AlarmDAOInt extends GenericDAO<Alarma, BigDecimal> {

    public List<Alarma> findAlarms();

    public List<Alarma> findAllAlarms();
}
