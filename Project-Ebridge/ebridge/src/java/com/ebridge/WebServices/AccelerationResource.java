/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.WebServices;

import com.ebridge.Logic.AccelerationManager;
import com.ebridge.Logic.SensorManager;
import com.ebridge.Entities.Aceleracion;
import com.ebridge.Entities.Sensor;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author melvinitcr
 */
@Path("acceleration")
public class AccelerationResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of AccelerationResource
     */
    public AccelerationResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.ebridge.WebServices.AccelerationResource
     *
     * @param axis
     * @param idtest
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Coordenate getJson(@QueryParam("axis") String axis, @QueryParam("idtest") int idtest) {

        Coordenate coordenate = null;

        try {
            if ("x".equals(axis)) {
                coordenate = new Coordenate("X Axis");
                List<Float> data = new ArrayList<>();
                AccelerationManager am = new AccelerationManager();

                List<Aceleracion> loadAllAccelerations = am.findByTest(idtest);

                for (Aceleracion loadAllAcceleration : loadAllAccelerations) {
                    data.add(loadAllAcceleration.getCoordenadaX());
                }
                coordenate.setData(data);
            }

            if ("y".equals(axis)) {
                coordenate = new Coordenate("Y Axis");
                List<Float> data = new ArrayList<>();
                AccelerationManager am = new AccelerationManager();
                List<Aceleracion> loadAllAccelerations = am.findByTest(idtest);
                for (Aceleracion loadAllAcceleration : loadAllAccelerations) {
                    data.add(loadAllAcceleration.getCoordenadaY());
                }
                coordenate.setData(data);
            }

            if ("z".equals(axis)) {
                coordenate = new Coordenate("Z Axis");
                List<Float> data = new ArrayList<>();
                AccelerationManager am = new AccelerationManager();
                List<Aceleracion> loadAllAccelerations = am.findByTest(idtest);

                for (Aceleracion loadAllAcceleration : loadAllAccelerations) {
                    data.add(loadAllAcceleration.getCoordenadaZ());
                }
                coordenate.setData(data);
            }
        } catch (Exception e) {

        }

        return coordenate;
    }

    /**
     * POST method for updating or creating an instance of AccelerationResource
     *
     * @param aceleracion
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String postJson(List<Aceleracion> aceleracion) {

        try {
            SensorManager sm = new SensorManager();
            Sensor sensor = sm.findSensorById(1);
            AccelerationManager am = new AccelerationManager();
            for (Aceleracion data : aceleracion) {
                data.setSensor(sensor);
                am.saveNewAcceleration(data);
            }
            return "succesfull";
        } catch (Exception e) {
            return "failure";
        }
    }
}