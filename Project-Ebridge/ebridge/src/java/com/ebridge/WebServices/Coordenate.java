/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.WebServices;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author melvinitcr
 */
public class Coordenate {

    private String name;
    private List<Float> data;

    public void setName(String name) {
        this.name = name;
    }

    public void setData(List<Float> data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public List<Float> getData() {
        return data;
    }

    public Coordenate() {
    }

    public Coordenate(String name) {
        this.name = name;
        data = new ArrayList<>();
    }

}
