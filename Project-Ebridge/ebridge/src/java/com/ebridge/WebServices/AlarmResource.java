/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.WebServices;

import com.ebridge.Entities.Alarma;
import com.ebridge.Logic.AlarmManager;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author melvinitcr
 */
@Path("alarm")
public class AlarmResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of AlarmResource
     */
    public AlarmResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.ebridge.WebServices.AlarmResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Path("getalarm")
    @Produces(MediaType.TEXT_PLAIN)
    public String getJson() {

        String data = null;
        try {

            AlarmManager as = new AlarmManager();
            List<Alarma> findAlarms = as.findAlarms();

            if (findAlarms.size() > 0) {
                findAlarms.get(0).setVista(Boolean.TRUE);
                as.updateAlarm(findAlarms.get(0));
                data = "alarm_detected";
            } else {
                data = "alarm_not_sdetected";
            }

        } catch (Exception e) {

        }
        return data;

    }

    @GET
    @Path("getalarms")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Alarma> getAlarms() {

        try {
            AlarmManager as = new AlarmManager();
            List<Alarma> findAlarms = as.findAllAlarms();
            return findAlarms;
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * PUT method for updating or creating an instance of AlarmResource
     *
     * @param alarma
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String postJson(Alarma alarma) {
        try {
            AlarmManager as = new AlarmManager();
            alarma.setVista(Boolean.FALSE);
            as.saveNewAlarm(alarma);
            return "succesfull";
        } catch (Exception e) {
            return "failure";
        }
    }
}
