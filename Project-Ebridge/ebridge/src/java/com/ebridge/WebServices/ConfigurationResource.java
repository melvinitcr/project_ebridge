/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.WebServices;

import com.ebridge.Entities.Configuracion;
import com.ebridge.Logic.ConfiguracionManager;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author melvinitcr
 */
@Path("configuration")
public class ConfigurationResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ConfigurationResource
     */
    public ConfigurationResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.ebridge.WebServices.ConfigurationResource
     *
     * @param device
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Configuracion getJson(@QueryParam("device") String device) {

        try {
            ConfiguracionManager cm = new ConfiguracionManager();
            List<Configuracion> configuration = cm.findBySensor(device);

            if (configuration.isEmpty()) {
                return new Configuracion(-1);
            } else {
                return configuration.get(0);
            }
        } catch (Exception e) {
            return new Configuracion(-1);
        }

    }

    /**
     * POST method for updating or creating an instance of ConfigurationResource
     *
     * @param content representation for the resource
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String postJson(Configuracion content) {

        try {
            ConfiguracionManager cm = new ConfiguracionManager();
            cm.saveNewConfiguration(content);
            return "Store on DB";
        } catch (Exception e) {
            return null;
        }

    }

    @GET
    @Path("update")
    @Produces(MediaType.TEXT_PLAIN)
    public String updateConfiguration(@QueryParam("idconfiguracion") int idconfiguracion) {

        try {
            ConfiguracionManager cm = new ConfiguracionManager();
            Configuracion conf = cm.findConfigurationById(idconfiguracion);

            if (conf != null) {
                conf.setRealizado(Boolean.TRUE);
                cm.updateConfiguration(conf);
                return "succesfull";
            } else {
                return "failure";
            }

        } catch (Exception e) {
            return "failure";
        }

    }
}
