/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.WebServices;

import com.ebridge.Entities.Estadistica;
import com.ebridge.Entities.Sensor;
import com.ebridge.Logic.SensorManager;
import com.ebridge.Logic.StatisticManager;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Melvin
 */
@Path("statistic")
public class StatisticResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of StatisticResource
     */
    public StatisticResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.ebridge.WebServices.StatisticResource
     *
     * @param idtest
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Estadistica> getJson(@QueryParam("idtest") int idtest) {

        try {
            StatisticManager stm = new StatisticManager();
            List<Estadistica> statistic = stm.findByTest(idtest);
            for (Estadistica findByTest1 : statistic) {
                findByTest1.setSensor(null);
            }
            return statistic;
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * POST method for updating or creating an instance of StatisticResource
     *
     * @param estadistica
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String postJson(Estadistica estadistica) {

        try {
            SensorManager sm = new SensorManager();
            Sensor sensor = sm.findSensorById(1);

            StatisticManager stm = new StatisticManager();
            estadistica.setSensor(sensor);
            stm.saveNewStatistic(estadistica);
            return "succesfull";

        } catch (Exception e) {
            return "failure";
        }
    }
}
