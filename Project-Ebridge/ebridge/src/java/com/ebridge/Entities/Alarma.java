package com.ebridge.Entities;
// Generated May 10, 2016 1:54:20 PM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Alarma generated by hbm2java
 */
@Entity
@Table(name = "alarma", schema = "public"
)
public class Alarma implements java.io.Serializable {

    private int idAlarma;
    private int nivel;
    private float coordenadaX;
    private float coordenadaY;
    private float coordenadaZ;
    private String mensaje;
    private int numeroSensor;
    private String device;
    private Date fecha;
    private Boolean vista;

    public Alarma() {
    }

    public Alarma(int idAlarma, int nivel, float coordenadaX, float coordenadaY, float coordenadaZ, String mensaje, int numeroSensor, String device, Date fecha) {
        this.idAlarma = idAlarma;
        this.nivel = nivel;
        this.coordenadaX = coordenadaX;
        this.coordenadaY = coordenadaY;
        this.coordenadaZ = coordenadaZ;
        this.mensaje = mensaje;
        this.numeroSensor = numeroSensor;
        this.device = device;
        this.fecha = fecha;
    }

    public Alarma(int idAlarma, int nivel, float coordenadaX, float coordenadaY, float coordenadaZ, String mensaje, int numeroSensor, String device, Date fecha, Boolean vista) {
        this.idAlarma = idAlarma;
        this.nivel = nivel;
        this.coordenadaX = coordenadaX;
        this.coordenadaY = coordenadaY;
        this.coordenadaZ = coordenadaZ;
        this.mensaje = mensaje;
        this.numeroSensor = numeroSensor;
        this.device = device;
        this.fecha = fecha;
        this.vista = vista;
    }

    @Id

    @Column(name = "id_alarma", unique = true, nullable = false)
    public int getIdAlarma() {
        return this.idAlarma;
    }

    public void setIdAlarma(int idAlarma) {
        this.idAlarma = idAlarma;
    }

    @Column(name = "nivel", nullable = false)
    public int getNivel() {
        return this.nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    @Column(name = "coordenada_x", nullable = false, precision = 8, scale = 8)
    public float getCoordenadaX() {
        return this.coordenadaX;
    }

    public void setCoordenadaX(float coordenadaX) {
        this.coordenadaX = coordenadaX;
    }

    @Column(name = "coordenada_y", nullable = false, precision = 8, scale = 8)
    public float getCoordenadaY() {
        return this.coordenadaY;
    }

    public void setCoordenadaY(float coordenadaY) {
        this.coordenadaY = coordenadaY;
    }

    @Column(name = "coordenada_z", nullable = false, precision = 8, scale = 8)
    public float getCoordenadaZ() {
        return this.coordenadaZ;
    }

    public void setCoordenadaZ(float coordenadaZ) {
        this.coordenadaZ = coordenadaZ;
    }

    @Column(name = "mensaje", nullable = false, length = 200)
    public String getMensaje() {
        return this.mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Column(name = "numero_sensor", nullable = false)
    public int getNumeroSensor() {
        return this.numeroSensor;
    }

    public void setNumeroSensor(int numeroSensor) {
        this.numeroSensor = numeroSensor;
    }

    @Column(name = "device", nullable = false)
    public String getDevice() {
        return this.device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha", nullable = false, length = 29)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Column(name = "vista")
    public Boolean getVista() {
        return this.vista;
    }

    public void setVista(Boolean vista) {
        this.vista = vista;
    }

}
