/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.Entities.Alarma;
import com.ebridge.Utils.HibernateUtil;
import com.ebridge.dao.Alarm.AlarmDAO;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author melvinitcr
 */
public class AlarmManager implements AlarmManagerInt {

    private final AlarmDAO alarmDAO = new AlarmDAO();

    @Override
    public List<Alarma> loadAllAlarms() {
        List<Alarma> allAlarms = new ArrayList<>();
        try {
            HibernateUtil.beginTransaction();
            allAlarms = alarmDAO.findAll(Alarma.class);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
        }
        return allAlarms;
    }

    @Override
    public void saveNewAlarm(Alarma alarma) {
        try {
            HibernateUtil.beginTransaction();
            alarmDAO.save(alarma);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
            HibernateUtil.rollbackTransaction();
        }
    }

    @Override
    public List<Alarma> findAlarms() {
        List<Alarma> list = null;
        try {
            HibernateUtil.beginTransaction();
            list = alarmDAO.findAlarms();
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here hola");
        }
        return list;
    }

    @Override
    public List<Alarma> findAllAlarms() {
        List<Alarma> list = null;
        try {
            HibernateUtil.beginTransaction();
            list = alarmDAO.findAllAlarms();
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here hola");
        }
        return list;
    }

    @Override
    public void updateAlarm(Alarma alarma) {
        try {
            HibernateUtil.beginTransaction();
            alarmDAO.update(alarma);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
            HibernateUtil.rollbackTransaction();
        }
    }

}
