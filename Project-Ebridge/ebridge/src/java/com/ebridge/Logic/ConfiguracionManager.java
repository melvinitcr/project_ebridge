/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.Entities.Aceleracion;
import com.ebridge.Entities.Configuracion;
import com.ebridge.Utils.HibernateUtil;
import com.ebridge.dao.Configuration.ConfigurationDAO;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author melvinitcr
 */
public class ConfiguracionManager implements ConfigurationManagerInt {

    private final ConfigurationDAO configurationDAO = new ConfigurationDAO();

    @Override
    public void saveNewConfiguration(Configuracion configuracion) {
        try {
            HibernateUtil.beginTransaction();
            configurationDAO.save(configuracion);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
            HibernateUtil.rollbackTransaction();
        }
    }

    @Override
    public Configuracion findConfigurationById(int id) {
        Configuracion configuracion = null;
        try {
            HibernateUtil.beginTransaction();
            configuracion = (Configuracion) configurationDAO.findByID(Configuracion.class, id);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
        }
        return configuracion;
    }

    
    @Override
    public void updateConfiguration(Configuracion configuracion) {
        try {
            HibernateUtil.beginTransaction();
            configurationDAO.update(configuracion);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
            HibernateUtil.rollbackTransaction();
        }
    }

    @Override
    public List<Configuracion> findBySensor(String device) {
        List<Configuracion> list = null;
        try {
            HibernateUtil.beginTransaction();
            list = configurationDAO.findBySensor(device);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here hola");
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

}
