/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.Entities.Estadistica;
import java.util.List;

/**
 *
 * @author Melvin
 */
public interface StatisticManagerInt {

    public List<Estadistica> loadAllStatistics();

    public void saveNewStatistic(Estadistica estadistica);

    public Estadistica findStatisticById(int id);

    public void deleteStatistic(Estadistica estadistica);

    public List<Estadistica> findByTest(int idtest);

}
