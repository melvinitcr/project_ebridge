/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.Entities.Configuracion;
import java.util.List;

/**
 *
 * @author melvinitcr
 */
public interface ConfigurationManagerInt {

    public void saveNewConfiguration(Configuracion configuracion);

    public Configuracion findConfigurationById(int id);

    public void updateConfiguration(Configuracion configuracion);

    public List<Configuracion> findBySensor(String Sensor);
}
