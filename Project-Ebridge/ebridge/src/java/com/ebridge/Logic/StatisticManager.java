/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.Entities.Estadistica;
import com.ebridge.Utils.HibernateUtil;
import com.ebridge.dao.StatisticDAO.StatisticDAO;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author Melvin
 */
public class StatisticManager implements StatisticManagerInt {

    private final StatisticDAO statisticsDAO = new StatisticDAO();

    @Override
    public List<Estadistica> loadAllStatistics() {
        List<Estadistica> allStatistics = new ArrayList<>();
        try {
            HibernateUtil.beginTransaction();
            allStatistics = statisticsDAO.findAll(Estadistica.class);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
        }
        return allStatistics;
    }

    @Override
    public void saveNewStatistic(Estadistica estadistica) {
        try {
            HibernateUtil.beginTransaction();
            statisticsDAO.save(estadistica);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
            System.out.println(ex.getMessage());
            HibernateUtil.rollbackTransaction();
        }
    }

    @Override
    public Estadistica findStatisticById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteStatistic(Estadistica estadistica) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Estadistica> findByTest(int idtest) {
        List<Estadistica> list = null;
        try {
            HibernateUtil.beginTransaction();
            list = statisticsDAO.findByTest(idtest);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
        }
        return list;
    }

}
