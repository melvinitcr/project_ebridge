/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.dao.Sensor.SensorDAO;
import com.ebridge.Entities.Sensor;
import com.ebridge.Utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author melvinitcr
 */
public class SensorManager implements SensorManagerInt {

    private final SensorDAO sensorDAO = new SensorDAO();

    @Override
    public List<Sensor> loadAllSensors() {
        List<Sensor> allSensors = new ArrayList<>();
        try {
            HibernateUtil.beginTransaction();
            allSensors = sensorDAO.findAll(Sensor.class);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
        }
        return allSensors;
    }

    @Override
    public void saveNewSensor(Sensor sensor) {
        try {
            HibernateUtil.beginTransaction();
            sensorDAO.save(sensor);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
            HibernateUtil.rollbackTransaction();
        }
    }

    @Override
    public Sensor findSensorById(int id) {
        Sensor sensor = null;
        try {
            HibernateUtil.beginTransaction();
            sensor = (Sensor) sensorDAO.findByID(Sensor.class, id);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
        }
        return sensor;
    }

    @Override
    public void deleteSensor(Sensor sensor) {
        try {
            HibernateUtil.beginTransaction();
            sensorDAO.delete(sensor);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
            HibernateUtil.rollbackTransaction();
        }
    }

}
