/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.Entities.Tiposensor;

/**
 *
 * @author melvinitcr
 */
public interface SensorTypeServiceInt {

    public Tiposensor findSensorTypeById(int id);

}
