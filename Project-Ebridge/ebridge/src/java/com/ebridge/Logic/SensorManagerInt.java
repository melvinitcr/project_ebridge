/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.Entities.Sensor;
import java.util.List;

/**
 *
 * @author melvinitcr
 */
public interface SensorManagerInt {

    public List<Sensor> loadAllSensors();

    public void saveNewSensor(Sensor sensor);

    public Sensor findSensorById(int id);

    public void deleteSensor(Sensor sensor);
}
