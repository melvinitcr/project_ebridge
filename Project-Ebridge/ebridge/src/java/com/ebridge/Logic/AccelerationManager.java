/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.dao.Acceleration.AccelerationDAO;
import com.ebridge.Entities.Aceleracion;
import com.ebridge.Utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author melvinitcr
 */
public class AccelerationManager implements AccelerationManagerInt {

    private final AccelerationDAO accelerationDAO = new AccelerationDAO();

    @Override
    public List<Aceleracion> loadAllAccelerations() {
        List<Aceleracion> allAccelerations = new ArrayList<>();
        try {
            HibernateUtil.beginTransaction();
            allAccelerations = accelerationDAO.findAll(Aceleracion.class);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
        }
        return allAccelerations;
    }

    @Override
    public void saveNewAcceleration(Aceleracion aceleracion) {
        try {
            HibernateUtil.beginTransaction();
            accelerationDAO.save(aceleracion);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
            System.out.println(ex.getMessage());
            HibernateUtil.rollbackTransaction();
        }
    }

    @Override
    public Aceleracion findAccelerationById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteAcceleration(Aceleracion aceleracion) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Aceleracion> findByTest(int idtest) {
        List<Aceleracion> list = null;
        try {
            HibernateUtil.beginTransaction();
            list = accelerationDAO.findByTest(idtest);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
