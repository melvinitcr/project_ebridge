/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.Entities.Alarma;
import java.util.List;

/**
 *
 * @author melvinitcr
 */
public interface AlarmManagerInt {

    public List<Alarma> loadAllAlarms();

    public void saveNewAlarm(Alarma alarma);

    public List<Alarma> findAlarms();

    public void updateAlarm(Alarma alarma);

    public List<Alarma> findAllAlarms();

}
