/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.Entities.Aceleracion;
import java.util.List;

/**
 *
 * @author melvinitcr
 */
public interface AccelerationManagerInt {

    public List<Aceleracion> loadAllAccelerations();

    public void saveNewAcceleration(Aceleracion aceleracion);

    public Aceleracion findAccelerationById(int id);

    public void deleteAcceleration(Aceleracion aceleracion);

    public List<Aceleracion> findByTest(int idtest);
}
