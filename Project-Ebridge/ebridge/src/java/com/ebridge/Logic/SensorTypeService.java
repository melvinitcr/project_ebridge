/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebridge.Logic;

import com.ebridge.dao.Sensortype.SensorTypeDAO;
import com.ebridge.Entities.Tiposensor;
import com.ebridge.Utils.HibernateUtil;
import org.hibernate.HibernateException;

/**
 *
 * @author melvinitcr
 */
public class SensorTypeService implements SensorTypeServiceInt {

    private final SensorTypeDAO sensorTypeDAO = new SensorTypeDAO();

    @Override
    public Tiposensor findSensorTypeById(int id) {
        Tiposensor tiposensor = null;
        try {
            HibernateUtil.beginTransaction();
            tiposensor = (Tiposensor) sensorTypeDAO.findByID(Tiposensor.class, id);
            HibernateUtil.commitTransaction();
        } catch (HibernateException ex) {
            System.out.println("Handle your error here");
        }
        return tiposensor;
    }

}
