/* 
 * File:   program.c
 * Author: Melvin Gutierrez V
 *
 * Created on May 1, 2016, 9:59 AM
 */

#include "ebridge_sensor.h"

void startProgram() {

    //CONFIGURATION
    Configuration conf;
    Message msg;

    int approval = getWebServiceAprobal(&conf, &msg, parameters.SERVICE_CONF);

    if (approval == 1) {

        printf("%s\n", "DATA ACQUISITION GET APPROVAL!");

        // TURN ON THE LED
        gpioWrite(25, 1); //C

        //CONFIGURE SAMPLING
        SAMPLES = 10 * conf.periodo;
        TESTNUMBER = conf.idprueba;
        SENSORNUMBER = conf.sensor;

        //PRINT CONFIGURATION
        printf("    Samples: %d Sensor:%d Test:%d \n", SAMPLES, SENSORNUMBER, TESTNUMBER);

        // SELECT SENSOR
        selectSensor(SENSORNUMBER);

        int isStadistic = conf.statistic;
        if (isStadistic == 1) {
            //TAKE STADISTICS
            printf("%s\n", "    Statistic Mode");
            take_and_send_stadistics(&msg, parameters.SERVICE_STD);
        } else if (isStadistic == 0) {
            //TAKE SAMPLES
            printf("%s\n", "    Sampling Mode");
            take_and_send_samples(&msg, parameters.SERVICE_ACCEL);
        }

        //UPDATE CONFIGURATION
        updateWebServiceConf(&msg, conf.idconfiguracion);

        // TURN OFF THE LED
        gpioWrite(25, 0); //C

    } else {
        printf("%s\n", "No request available!");
    }

}

void start_Alarm_Indicators() {

    timerStart();

    while (1) { // Always checking

        int n_sensor;
        for (n_sensor = 0; n_sensor < parameters.TOTAL_SENSORS; ++n_sensor) { // Iterates on each sensor connected to the device
            selectSensor(n_sensor); // Selects a sensor
            inspectData(n_sensor); // Inspects the data from that sensor
            usleep(100000); // Waits 100 ms
        }

        if (timerStop() > parameters.CHECK_TIME) { //Every 10 seconds the application checks for on-demand data acquisition.
            startProgram();
            timerStart();
        }
    }

}

int main(int argc, char** argv) {

    //Program running led indicator
    gpioWrite(26, 1); //C

    //Parameters
    getParameters();

    // Setup Wiring Pi
    wiringPiSetup();

    // Start Sensors
    start_Sensors();

    //Start Alarms & Indicators
    start_Alarm_Indicators();

}
