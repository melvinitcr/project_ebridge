#include "ebridge_sensor.h"

/**
 * This method iterates through the sensors connected to the device 
 * to configure them
 */
void start_Sensors() {

    //Default sensor
    selectSensor(0);

    //OPEN CONNECTION
    fd = openConnection_to_Device();

    int i;
    for (i = 0; i < 8; ++i) {
        // SELECT SENSOR
        selectSensor(i);

        //Sleep
        usleep(100000);

        //CONFIGURE DEVICE
        configureDevice(fd);

        //Sleep
        usleep(100000);
    }
}

/**
 * This method reads the values x, y & z from a sensor, assesses these values
 * regarding the maximum and minimum values for each axis, if the values read
 * exceed the limit of the maximum and minimum values, then an alarm is created
 * which is sent to the WebService.
 * @param sensor -  Number of the sensor
 */
void inspectData(int sensor) {

    int alarm_detected = 0; // Flag variable

    float x = (getAccelX() * parameters.AccelerationFactor); // Reads X
    float y = (getAccelY() * parameters.AccelerationFactor); // Reads Y
    float z = (getAccelZ() * parameters.AccelerationFactor); // Reads Z

    char *message = malloc(250); // Message for storing alarms information
    strcpy(message, "-");

    if (x < parameters.ALERT_MIN_X) { //Lower limit x axis 
        printf("Bridge is down, X min!\n");
        strcat(message, "Bridge is down, X min!	");
        printf("%f\n", x);
        alarm_detected = 1;
    }

    if (x > parameters.ALERT_MAX_X) {// Upper limit x axis
        printf("Bridge is down, X max!\n");
        strcat(message, "Bridge is down, X max!	");
        printf("%f\n", x);
        alarm_detected = 1;
    }

    if (y < parameters.ALERT_MIN_Y) {//Lower limit y axis 
        printf("Bridge is down, Y min!\n");
        strcat(message, "Bridge is down, Y min!	");
        printf("%f\n", y);
        alarm_detected = 1;
    }

    if (y > parameters.ALERT_MAX_Y) {//Upper limit y axis
        printf("Bridge is down, Y max!\n");
        strcat(message, "Bridge is down, Y max!	");
        printf("%f\n", y);
        alarm_detected = 1;
    }

    if (z < parameters.ALERT_MIN_Z) {//Lower limit z axis 
        printf("Bridge is down, Z min!\n");
        strcat(message, "Bridge is down, Z min!	");
        printf("%f\n", z);
        alarm_detected = 1;
    }

    if (z > parameters.ALERT_MAX_Z) {//Upper limit z axis
        printf("Bridge is down, Z max!\n");
        strcat(message, "Bridge is down, Z max!	");
        printf("%f\n", z);
        alarm_detected = 1;
    }

    if (alarm_detected == 1) { // If an limit value is reach "alarm_detected" equals 1

        // Get timestamp
        char *date_and_time = malloc(30);
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        snprintf(date_and_time, 30, "%d-%02d-%02dT%02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

        Message msg;
        json_object *JSON = json_object_new_object(); // JSON object for storing alarm information

        transform_Alarm_into_JSON(JSON, 0, 5, x, y, z, message, sensor, parameters.DEVICE_ID, date_and_time);

        sendWebServiceJSON(&msg, JSON, parameters.SERVICE_ALARM);

        free(date_and_time); // Free memory date
    }
    free(message); // Free memory message
}

/**
 * This method is responsible for sending a JSON object type to a WebService,
 * the information related its stored in the Log File.
 * This method is called by Threads.
 * @param args
 * @return 
 */
void *send_over_thread(void *args) {

    json_object *JSON = args;
    Message msg;
    sendJSONData(&msg, JSON, parameters.SERVICE_ACCEL);

    if (msg.code == SUCC) {
        if (strcmp(msg.message, "failure") == 0) {
            printf("%s\n", "    The JSON data could not be saved in the DB");
            Log("The JSON data could not be saved in the DB", EJNNS);

        } else if (strcmp(msg.message, "succesfull") == 0) {
            printf("%s\n", "    The JSON data was saved in the DB");
            Log("The JSON data was saved in the DB", 0);

        } else {
            printf("%s\n", "     Information from the webservice return as an error");
            Log("Information return as an error", EIRAE);
        }

    } else {
        printf("Error in accessing the webservice error code: %d \n", msg.code);
        Log("Error in accessing the webservice", msg.code);
    }
    free(msg.message); // free message data
}

/**
 * This method obtains a sequence of sample blocks indicated by the SAMPLES
 * variable, each sample block is added to a JSON object type and then is send
 * to the WebService.
 * @param msg
 * @param url
 */
void take_and_send_samples(Message* msg, char* url) {

    int block = parameters.BLOCK; // Get the number of samples by block

    int num_threads = SAMPLES / parameters.BLOCK; // Set the numbers of threads necessary to perform this functionality
    pthread_t primes[num_threads];
    int count_thread = 0;

    while (SAMPLES > 0) {

        if (SAMPLES < block) {

            json_object *JSON = json_object_new_array(); // Create JSON object type
            getSamples(JSON, SAMPLES);
            sendWebServiceJSON(msg, JSON, url); // Send JSON to WebService
            break;
        } else {

            json_object *JSON = json_object_new_array(); // Create JSON object type
            getSamples(JSON, block);
            SAMPLES = SAMPLES - block; // Decrease the number of blocks remaining

            if (pthread_create(&primes[count_thread], NULL, send_over_thread, JSON) == 0) {// Send JSON to WebService
                printf("%s\n", "            thread created");
                count_thread++; // Next thread
            }
        }
    }
    printf("%s\n", "finish");
    int i;
    for (i = 0; i < num_threads; ++i) {
        // wait for the thread to finish
        if (pthread_join(primes[i], NULL)) {
            fprintf(stderr, "Error joining thread\n");
        }
    }
}

/**
 * This method gets statistic data from the accelerometer sensor. The statistic data
 * is get from a number of samples given by the variable SAMPLES.
 * @param msg
 * @param url
 */
void take_and_send_stadistics(Message* msg, char* url) {

    Statistic stadistic; // Instantiate statistic structure
    getStadistics(SAMPLES, &stadistic);

    json_object *JSON = json_object_new_object(); // Create JSON object

    // Get current date and time
    char *date_and_time = malloc(30);
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    snprintf(date_and_time, 30, "%d-%02d-%02dT%02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

    transform_into_JSON(&stadistic, JSON, 0, TESTNUMBER, date_and_time);
    sendWebServiceJSON(msg, JSON, url);

    free(date_and_time); // Free memory from date_and_time
}

/**
 * This method is responsible for sending a JSON object type to a WebService,
 * if an error occurs, it is stored in the Log File the information related
 * to the error.
 * @param msg
 * @param JSON
 * @param url
 */
void sendWebServiceJSON(Message* msg, json_object *JSON, char* url) {

    sendJSONData(msg, JSON, url);
    if (msg->code == SUCC) {

        if (strcmp(msg->message, "failure") == 0) {
            printf("%s\n", "    The JSON data could not be saved in the DB");
            Log("The JSON data could not be saved in the DB", EJNNS);
        } else if (strcmp(msg->message, "succesfull") == 0) {
            printf("%s\n", "    The JSON data was saved in the DB");
            Log("The JSON data was saved in the DB", 0);
        } else {
            printf("%s\n", "     Information from the webservice return as an error");
            Log("Information return as an error", EIRAE);
        }

    } else {
        printf("Error in accessing the webservice error code: %d \n", msg->code);
        Log("Error in accessing the webservice", msg->code);
    }
    free(msg->message); // Free message data
}

/**
 * This method transforms the  statistical data into a single JSON object type,
 * which then will be sent to the WebService.
 * @param stadistic
 * @param jobj
 * @param id_estadistica
 * @param idprueba
 * @param fecha
 */
void transform_into_JSON(Statistic* stadistic, json_object* jobj, int id_estadistica, int idprueba, char* fecha) {

    char* char_promedio_x = "promedioX";
    char* char_promedio_y = "promedioY";
    char* char_promedio_z = "promedioZ";
    char* char_varianza_x = "varianzaX";
    char* char_varianza_y = "varianzaY";
    char* char_varianza_z = "varianzaZ";
    char* char_std_dev_x = "stdDevX";
    char* char_std_dev_y = "stdDevY";
    char* char_std_dev_z = "stdDevZ";
    char* char_max_x = "maxX";
    char* char_max_y = "maxY";
    char* char_max_z = "maxZ";
    char* char_min_x = "minX";
    char* char_min_y = "minY";
    char* char_min_z = "minZ";
    char* char_idprueba = "idprueba";
    char* char_id_estadistica = "idEstadistica";
    char* char_fecha = "fecha";


    //Creating json integers
    json_object *jint_id_estadistica = json_object_new_int(id_estadistica);
    json_object *jint_idprueba = json_object_new_int(idprueba);

    //Creating json doubles
    json_object *jdouble_average_x = json_object_new_double(stadistic->average_x);
    json_object *jdouble_average_y = json_object_new_double(stadistic->average_y);
    json_object *jdouble_average_z = json_object_new_double(stadistic->average_z);

    json_object *jdouble_variance_x = json_object_new_double(stadistic->variance_x);
    json_object *jdouble_variance_y = json_object_new_double(stadistic->variance_y);
    json_object *jdouble_variance_z = json_object_new_double(stadistic->variance_z);

    json_object *jdouble_std_dev_x = json_object_new_double(stadistic->std_dev_x);
    json_object *jdouble_std_dev_y = json_object_new_double(stadistic->std_dev_y);
    json_object *jdouble_std_dev_z = json_object_new_double(stadistic->std_dev_z);

    json_object *jdouble_max_x = json_object_new_double(stadistic->max_x);
    json_object *jdouble_max_y = json_object_new_double(stadistic->max_y);
    json_object *jdouble_max_z = json_object_new_double(stadistic->max_z);

    json_object *jdouble_min_x = json_object_new_double(stadistic->min_x);
    json_object *jdouble_min_y = json_object_new_double(stadistic->min_y);
    json_object *jdouble_min_z = json_object_new_double(stadistic->min_z);

    //Creating json string
    json_object *jstring_fecha = json_object_new_string(fecha);

    //Form the json object
    //Each of these is like a key value pair
    json_object_object_add(jobj, char_promedio_x, jdouble_average_x);
    json_object_object_add(jobj, char_promedio_y, jdouble_average_y);
    json_object_object_add(jobj, char_promedio_z, jdouble_average_z);

    json_object_object_add(jobj, char_varianza_x, jdouble_variance_x);
    json_object_object_add(jobj, char_varianza_y, jdouble_variance_y);
    json_object_object_add(jobj, char_varianza_z, jdouble_variance_z);

    json_object_object_add(jobj, char_std_dev_x, jdouble_std_dev_x);
    json_object_object_add(jobj, char_std_dev_y, jdouble_std_dev_y);
    json_object_object_add(jobj, char_std_dev_z, jdouble_std_dev_z);

    json_object_object_add(jobj, char_max_x, jdouble_max_x);
    json_object_object_add(jobj, char_max_y, jdouble_max_y);
    json_object_object_add(jobj, char_max_z, jdouble_max_z);

    json_object_object_add(jobj, char_min_x, jdouble_min_x);
    json_object_object_add(jobj, char_min_y, jdouble_min_y);
    json_object_object_add(jobj, char_min_z, jdouble_min_z);

    json_object_object_add(jobj, char_idprueba, jint_idprueba);
    json_object_object_add(jobj, char_id_estadistica, jint_id_estadistica);

    json_object_object_add(jobj, char_fecha, jstring_fecha);

}

/**
 * This method transforms the various parameters of an alarm in a single
 * JSON object type, which then will be sent to the WebService.
 * @param jobj
 * @param idAlarma
 * @param nivel
 * @param coordenadaX
 * @param coordenadaY
 * @param coordenadaZ
 * @param mensaje
 * @param numeroSensor
 * @param device
 * @param fecha
 */
void transform_Alarm_into_JSON(json_object* jobj, int idAlarma, int nivel,
        float coordenadaX, float coordenadaY, float coordenadaZ, char* mensaje,
        int numeroSensor, char* device, char* fecha) {

    char* char_nivel = "nivel";
    char* char_coordenadaX = "coordenadaX";
    char* char_coordenadaY = "coordenadaY";
    char* char_coordenadaZ = "coordenadaZ";
    char* char_mensaje = "mensaje";
    char* char_numeroSensor = "numeroSensor";
    char* char_device = "device";
    char* char_fecha = "fecha";
    char* char_idAlarma = "idAlarma";

    //Creating json integers
    json_object *jint_idAlarma = json_object_new_int(idAlarma);
    json_object *jint_nivel = json_object_new_int(nivel);
    json_object *jint_numeroSensor = json_object_new_int(numeroSensor);

    //Creating json doubles
    json_object *jdouble_coordenadaX = json_object_new_double(coordenadaX);
    json_object *jdouble_coordenadaY = json_object_new_double(coordenadaY);
    json_object *jdouble_coordenadaZ = json_object_new_double(coordenadaZ);

    //Creating json string
    json_object *jstring_fecha = json_object_new_string(fecha);
    json_object *jstring_device = json_object_new_string(device);
    json_object *jstring_mensaje = json_object_new_string(mensaje);

    //Form the json object
    //Each of these is like a key value pair
    json_object_object_add(jobj, char_nivel, jint_nivel);
    json_object_object_add(jobj, char_coordenadaX, jdouble_coordenadaX);
    json_object_object_add(jobj, char_coordenadaY, jdouble_coordenadaY);
    json_object_object_add(jobj, char_coordenadaZ, jdouble_coordenadaZ);

    json_object_object_add(jobj, char_mensaje, jstring_mensaje);
    json_object_object_add(jobj, char_numeroSensor, jint_numeroSensor);

    json_object_object_add(jobj, char_device, jstring_device);
    json_object_object_add(jobj, char_fecha, jstring_fecha);
    json_object_object_add(jobj, char_idAlarma, jint_idAlarma);
}

void configIdsetup(char* urlConfig, int idconfiguracion) {

    strcpy(urlConfig, parameters.SERVICE_CONF_UP);
    char int_char[100];
    sprintf(int_char, "%d", idconfiguracion);
    strcat(urlConfig, int_char);
}

/**
 * This method writes a value into the GPIO pin register of the device 
 * @param pin
 * @param value
 */
void gpioWrite(int pin, int value) {

    char array4[100] = "echo ";
    char svalue[30];
    sprintf(svalue, "%d", value);
    strcat(array4, svalue);
    strcat(array4, " > /sys/class/gpio/gpio");
    char spin[30];
    sprintf(spin, "%d", pin);
    strcat(array4, spin);
    strcat(array4, "/value");
    system(array4);
}

/**
 * This method selects one sensor connected to the device by an integer
 * given by the sensor variable
 * @param sensor
 */
void selectSensor(int sensor) {

    //High Voltage = 3.3 V
    int H = 1;
    //Low Voltage = 0.0 V
    int L = 0;

    if (sensor == 0) {
        gpioWrite(parameters.GPIOC, L); //C
        gpioWrite(parameters.GPIOB, L); //B
        gpioWrite(parameters.GPIOA, L); //A
    } else if (sensor == 1) {
        gpioWrite(parameters.GPIOC, L); //C
        gpioWrite(parameters.GPIOB, L); //B
        gpioWrite(parameters.GPIOA, H); //A
    } else if (sensor == 2) {
        gpioWrite(parameters.GPIOC, L); //C
        gpioWrite(parameters.GPIOB, H); //B
        gpioWrite(parameters.GPIOA, L); //A
    } else if (sensor == 3) {
        gpioWrite(parameters.GPIOC, L); //C
        gpioWrite(parameters.GPIOB, H); //B
        gpioWrite(parameters.GPIOA, H); //A
    } else if (sensor == 4) {
        gpioWrite(parameters.GPIOC, H); //C
        gpioWrite(parameters.GPIOB, L); //B
        gpioWrite(parameters.GPIOA, L); //A
    } else if (sensor == 5) {
        gpioWrite(parameters.GPIOC, H); //C
        gpioWrite(parameters.GPIOB, L); //B
        gpioWrite(parameters.GPIOA, H); //A
    } else if (sensor == 6) {
        gpioWrite(parameters.GPIOC, H); //C
        gpioWrite(parameters.GPIOB, H); //B
        gpioWrite(parameters.GPIOA, L); //A
    } else if (sensor == 7) {
        gpioWrite(parameters.GPIOC, H); //C
        gpioWrite(parameters.GPIOB, H); //B
        gpioWrite(parameters.GPIOA, H); //A
    }
}

/**
 * This method stores into an array of coordinates a certain amount of samples,
 * each sample is take every WAIT_TIME ms, later statistical data is calculated for
 * the x, y & z axis.
 * @param samples
 * @param stadistic
 */
void getStadistics(int samples, Statistic* stadistic) {

    Coordenate array[samples];

    int i;
    for (i = 0; i < samples; i++) {

        //DATA ADQUISITION
        short accelX = getAccelX();
        short accelY = getAccelY();
        short accelZ = getAccelZ();

        float xa = (accelX * parameters.AccelerationFactor);
        float ya = (accelY * parameters.AccelerationFactor);
        float za = (accelZ * parameters.AccelerationFactor);

        Coordenate ft; // Coordenate structure
        ft.x = xa;
        ft.y = ya;
        ft.z = za;
        array[i] = ft;

        usleep(parameters.WAIT_TIME); // one sample each WAIT_TIME ms
    }

    getStdX(array, samples, stadistic);
    getStdY(array, samples, stadistic);
    getStdZ(array, samples, stadistic);

}

/**
 * This method receives an array of coordinates and calculates statistical data
 * for the X axis
 * @param array
 * @param samples
 * @param stadistic
 */
void getStdX(Coordenate* array, int samples, Statistic* stadistic) {

    //Initiate local variables
    float sum = 0.0;
    float sum1 = 0.0;
    float average = 0.0;
    float std_deviation = 0.0;
    float variance = 0.0;
    float max = 0.0;
    float min = 1000.0;
    int i = 0;

    //  Compute the sum of all elements
    for (i = 0; i < samples; i++) {
        sum = sum + array[i].x;
    }

    average = sum / (float) samples;
    //  Compute  variance  and standard deviation
    for (i = 0; i < samples; i++) {
        sum1 = sum1 + pow((array[i].x - average), 2);
    }

    // Compute the max and min of all elements
    for (i = 0; i < samples; i++) {
        float b = fabs(array[i].x);
        if (b > max) {
            max = b;
        }
        if (b < min) {
            min = b;
        }
    }

    variance = sum1 / (float) samples; // Compute the the variance
    std_deviation = sqrt(variance); // Compute the standard deviation


    // Fill de coordenate structure
    stadistic->average_x = average;
    stadistic->variance_x = variance;
    stadistic->std_dev_x = std_deviation;
    stadistic->max_x = max;
    stadistic->min_x = min;

}

/**
 * This method receives an array of coordinates and calculates statistical data
 * for the Y axis
 * @param array
 * @param samples
 * @param stadistic
 */
void getStdY(Coordenate* array, int samples, Statistic* stadistic) {

    float sum = 0.0;
    float sum1 = 0.0;
    float average = 0.0;
    float std_deviation = 0.0;
    float variance = 0.0;
    float max = 0.0;
    float min = 1000.0;
    int i = 0;

    //  Compute the sum of all elements
    for (i = 0; i < samples; i++) {
        sum = sum + array[i].y;
    }

    average = sum / (float) samples;
    //  Compute  variance  and standard deviation
    for (i = 0; i < samples; i++) {
        sum1 = sum1 + pow((array[i].y - average), 2);
    }


    //  Compute the max of all elements
    for (i = 0; i < samples; i++) {
        float b = fabs(array[i].y);
        if (b > max) {
            max = b;
        }
        if (b < min) {
            min = b;
        }
    }

    variance = sum1 / (float) samples;
    std_deviation = sqrt(variance);

    stadistic->average_y = average;
    stadistic->variance_y = variance;
    stadistic->std_dev_y = std_deviation;
    stadistic->max_y = max;
    stadistic->min_y = min;

}

/**
 * This method receives an array of coordinates and calculates statistical data
 * for the Z axis
 * @param array
 * @param samples
 * @param stadistic
 */
void getStdZ(Coordenate* array, int samples, Statistic* stadistic) {

    float sum = 0.0;
    float sum1 = 0.0;
    float average = 0.0;
    float std_deviation = 0.0;
    float variance = 0.0;
    float max = 0.0;
    float min = 1000.0;
    int i = 0;

    //  Compute the sum of all elements
    for (i = 0; i < samples; i++) {
        sum = sum + array[i].z;
    }

    average = sum / (float) samples;
    //  Compute  variance  and standard deviation
    for (i = 0; i < samples; i++) {
        sum1 = sum1 + pow((array[i].z - average), 2);
    }


    //  Compute the max of all elements
    for (i = 0; i < samples; i++) {
        float b = fabs(array[i].z);
        if (b > max) {
            max = b;
        }
        if (b < min) {
            min = b;
        }
    }

    variance = sum1 / (float) samples;
    std_deviation = sqrt(variance);

    stadistic->average_z = average;
    stadistic->variance_z = variance;
    stadistic->std_dev_z = std_deviation;
    stadistic->max_z = max;
    stadistic->min_z = min;

}

/**
 * This method is used to obtain a configuration from the database,
 * based on the device ID
 * @param conf
 * @param msg
 * @param url
 * @return 
 */
int getWebServiceAprobal(Configuration* conf, Message* msg, char* url) {

    requestConfigurationJSON(msg, url);

    if (msg->code == SUCC) {

        enum json_tokener_error jerr = json_tokener_success; // json parse error
        json_object *json = json_object_new_object();
        json = json_tokener_parse_verbose(msg->message, &jerr);

        // check error
        if (jerr != json_tokener_success) {
            fprintf(stderr, "Error: Failed to parse JSON");
            Log("Failed to parse information to JSON format", EFPJSON);
            json_object_put(json);
            free(msg->message);
            return 0;

        } else {//no error
            json_parse(conf, json);
            if (conf->idconfiguracion != -1) {
                free(msg->message);
                return 1;
            }
        }

    } else {
        free(msg->message); // Free the memory allocated by the Message stream 
        Log("Error in accessing the webservice", msg->code);
        return 0;
    }

}

void checkRC(int rc, char *text) {
    if (rc < 0) {
        printf("Error: %s - %d\n"); //agregar error
        Log("Could not connect to the sensor", ECNCS);
        exit(-1);
    }
}

/**
 * This method opens a connection to a sensor
 * @return File Descriptor
 */
int openConnection_to_Device() {

    // Open an I2C connection
    int fda = wiringPiI2CSetup(MPU6050_ADDRESS);
    checkRC(fda, "wiringPiI2CSetup");
    return fda;
}

/*
 *          |   ACCELEROMETER    |
 * DLPF_CFG | Bandwidth | Delay  |  Sample Rate
 * ---------+-----------+--------+------------------
 * 0        | 260Hz     | 0ms    | 8kHz
 * 1        | 184Hz     | 2.0ms  | 1kHz
 * 2        | 94Hz      | 3.0ms  | 1kHz
 * 3        | 44Hz      | 4.9ms  | 1kHz
 * 4        | 21Hz      | 8.5ms  | 1kHz
 * 5        | 10Hz      | 13.8ms | 1kHz
 * 6        | 5Hz       | 19.0ms | 1kHz
 * 7        |   -- Reserved --   | Reserved
 */

/**
 * This method sets up a sensor from a file descriptor fda, the settings 
 * are made by writing to internal registers of the sensor.
 * @param fda
 */
void configureDevice(int fda) {

    // Perform I2C work
    wiringPiI2CWriteReg8(fda, MPU6050_REG_PWR_MGMT_1, 0);

    //Disable gyro self tests, scale of 500 degrees/s
    wiringPiI2CWriteReg8(fda, MPU6050_RA_GYRO_CONFIG, 0x0b00001000);

    //Disable accelerometer self tests, scale of +-8g, no DHPF
    wiringPiI2CWriteReg8(fda, MPU6050_RA_ACCEL_CONFIG, 0x10);

    // Get filter value form parameter configuration.
    int filter = parameters.FILTER_TYPE;

    if (filter == 0) {
        wiringPiI2CWriteReg8(fda, MPU6050_CONFIG, 0x00); //DLPF set = f=260Hz
    } else if (filter == 1) {
        wiringPiI2CWriteReg8(fda, MPU6050_CONFIG, 0x01); //DLPF set = f=184Hz
    } else if (filter == 2) {
        wiringPiI2CWriteReg8(fda, MPU6050_CONFIG, 0x02); //DLPF set = f=94Hz
    } else if (filter == 3) {
        wiringPiI2CWriteReg8(fda, MPU6050_CONFIG, 0x03); //DLPF set = f=44Hz
    } else if (filter == 4) {
        wiringPiI2CWriteReg8(fda, MPU6050_CONFIG, 0x04); //DLPF set = f=21Hz
    } else if (filter == 5) {
        wiringPiI2CWriteReg8(fda, MPU6050_CONFIG, 0x05); //DLPF set = f=10Hz
    } else if (filter == 6) {
        wiringPiI2CWriteReg8(fda, MPU6050_CONFIG, 0x06); //DLPF set = f=5Hz
    }
}

/**
 * This method performs a data reading from X axis, is performed by reading 
 * two 8-bit registers within the sensor.
 * @return  X axis value
 */
short getAccelX() {

    uint8_t msb = wiringPiI2CReadReg8(fd, MPU6050_REG_DATA_START);
    uint8_t lsb = wiringPiI2CReadReg8(fd, MPU6050_REG_DATA_START + 1);
    short accelX = msb << 8 | lsb;
    return accelX;
}

/**
 * This method performs a data reading from Y axis, is performed by reading 
 * two 8-bit registers within the sensor.
 * @return  Y axis value
 */
short getAccelY() {

    uint8_t msb = wiringPiI2CReadReg8(fd, MPU6050_REG_DATA_START + 2);
    uint8_t lsb = wiringPiI2CReadReg8(fd, MPU6050_REG_DATA_START + 3);
    short accelY = msb << 8 | lsb;
    return accelY;
}

/**
 * This method performs a data reading from Z axis, is performed by reading 
 * two 8-bit registers within the sensor.
 * @return  Z axis value
 */
short getAccelZ() {

    uint8_t msb = wiringPiI2CReadReg8(fd, MPU6050_REG_DATA_START + 4);
    uint8_t lsb = wiringPiI2CReadReg8(fd, MPU6050_REG_DATA_START + 5);
    short accelZ = msb << 8 | lsb;
    return accelZ;
}

/**
 * This method takes a certain amount of samples given by the variable
 * SAMPLE_SECTOR of the acceleration data in the x, y & z axis, each sample is 
 * added to a file with extention CSV and is also added to an array of samples
 * within an object JSON type.
 * @param JSON
 * @param SAMPLE_SECTOR
 */
void getSamples(json_object *JSON, int SAMPLE_SECTOR) {

    printf("%s\n", "                                                    Start sampling");

    //system("touch file.csv");
    int samples = 0;

    //FILE *f = fopen("file.csv", "w");
    // if (f == NULL) {
    // printf("Error opening file!\n");
    // exit(1);
    // }

    char *date_and_time = malloc(30); // Date

    while (samples < SAMPLE_SECTOR) {

        samples += 1; // Increase number the number of samples took

        //DATA ADQUISITION
        short accelX = getAccelX(); // Acceleration in X
        short accelY = getAccelY(); // Acceleration in Y
        short accelZ = getAccelZ(); // Acceleration in Z

        // PRINT INFO TO FILE
        float xa = (accelX * parameters.AccelerationFactor);
        float ya = (accelY * parameters.AccelerationFactor);
        float za = (accelZ * parameters.AccelerationFactor);

        //fprintf(f, "%f, %f, %f\n", xa, ya, za);

        //TO JSON FILE
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        snprintf(date_and_time, 30, "%d-%02d-%02dT%02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
        addDataJSON(JSON, xa, ya, za, 0, TESTNUMBER, date_and_time);

        usleep(parameters.WAIT_TIME); // one sample each WAIT_TIME milliseconds

    }
    free(date_and_time);
    // fclose(f);
}

/**
 * This method sends a file to a FTP Server
 */
void sendSamplesToFTP() {

    system("./ftp_upload.sh file.csv");
    printf("%s\n", "    Data send to FTP Server");
}

size_t curl_callback(void *contents, size_t size, size_t nmemb, void *userp) {

    size_t realsize = size * nmemb; // calculate buffer size
    struct curl_fetch_st *p = (struct curl_fetch_st *) userp; // cast pointer to fetch struct

    // expand buffer
    p->payload = (char *) realloc(p->payload, p->size + realsize + 1);

    // check buffer
    if (p->payload == NULL) {
        // this isn't good
        fprintf(stderr, "ERROR: Failed to expand buffer in curl_callback");
        // free buffer
        free(p->payload);
        return EFEBCURL;
    }

    //copy contents to buffer
    memcpy(&(p->payload[p->size]), contents, realsize);

    // set new buffer size
    p->size += realsize;

    // ensure null termination
    p->payload[p->size] = 0;

    //return size
    return realsize;
}

/**
 * This method fetch and return url body via curl
 * @param ch
 * @param url
 * @param fetch
 * @return 
 */
CURLcode curl_fetch_url(CURL *ch, const char *url, struct curl_fetch_st *fetch) {

    CURLcode rcode; // curl result code

    // init payload
    fetch->payload = (char *) calloc(1, sizeof (fetch->payload));

    // check payload
    if (fetch->payload == NULL) {
        // log error
        fprintf(stderr, "ERROR: Failed to allocate payload in curl_fetch_url");
        // return error
        return CURLE_FAILED_INIT;
    }

    // init size
    fetch->size = 0;

    // set url to fetch
    curl_easy_setopt(ch, CURLOPT_URL, url);

    // set calback function
    curl_easy_setopt(ch, CURLOPT_WRITEFUNCTION, curl_callback);

    // pass fetch struct pointer
    curl_easy_setopt(ch, CURLOPT_WRITEDATA, (void *) fetch);

    // set default user agent
    curl_easy_setopt(ch, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    // set timeout
    curl_easy_setopt(ch, CURLOPT_TIMEOUT, 240); // set to 240 seconds

    // enable location redirects
    curl_easy_setopt(ch, CURLOPT_FOLLOWLOCATION, 1);

    // set maximum allowed redirects
    curl_easy_setopt(ch, CURLOPT_MAXREDIRS, 1);

    // fetch the url
    rcode = curl_easy_perform(ch);

    // return
    return rcode;
}

/**
 * This method is responsible for sending a JSON object type to a WebService,
 * the response from the WebService is stored within the Message structure
 * The curl library is use to perform this functionality
 * @param msg
 * @param JSON
 * @param url
 * @return 
 */
int sendJSONData(Message* msg, json_object *JSON, char* url) {

    CURL *ch; // curl handle
    CURLcode rcode; // curl result code

    struct curl_fetch_st curl_fetch; // curl fetch struct
    struct curl_fetch_st *cf = &curl_fetch; // pointer to fetch struct
    struct curl_slist *headers = NULL; // http headers to send with request

    //Memory allocation
    char* msg_aux = (char *) malloc(sizeof (char));

    // init curl handle
    if ((ch = curl_easy_init()) == NULL) {
        // log error
        fprintf(stderr, "ERROR: Failed to create curl handle in fetch_session");
        // return error
        msg->code = ECCHFS;
        msg->message = msg_aux;
        return;
    }

    // set content type
    headers = curl_slist_append(headers, "Accept: application/json, text/javascript, */*; q=0.01");
    headers = curl_slist_append(headers, "Content-Type: application/json");

    // set curl options
    curl_easy_setopt(ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_easy_setopt(ch, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(ch, CURLOPT_POSTFIELDS, json_object_to_json_string(JSON));

    // fetch page and capture return code 
    rcode = curl_fetch_url(ch, url, cf);

    // cleanup curl handle 
    curl_easy_cleanup(ch);

    // free headers 
    curl_slist_free_all(headers);

    // free json object 
    json_object_put(JSON);

    // check return code
    if (rcode != CURLE_OK || cf->size < 1) {
        // log error 
        fprintf(stderr, "ERROR: Failed to fetch url (%s) - curl said: %s", url, curl_easy_strerror(rcode));
        // return error
        msg->code = EFFURL;
        msg->message = msg_aux;
        return;
    }

    // check payload 
    if (cf->payload != NULL) {
        msg->message = cf->payload;
        msg->code = SUCC;
        free(msg_aux);
    } else {
        fprintf(stderr, "ERROR: Failed to populate payload");
        // free payload 
        free(cf->payload);
        // return
        msg->code = EFPP;
        msg->message = msg_aux;
        return;
    }
}

/**
 * This method transforms the acceleration data of a single sample into a single
 * JSON object type, which then will be sent to the WebService.
 * @param jarray
 * @param coordenadaX
 * @param coordenadaY
 * @param coordenadaZ
 * @param idaceleracion
 * @param idprueba
 * @param fecha
 */
void addDataJSON(json_object *jarray, float coordenadaX, float coordenadaY,
        float coordenadaZ, int idaceleracion, int idprueba, char* fecha) {

    //Creating a json object
    json_object *jobj = json_object_new_object();

    char* char_coordenadaX = "coordenadaX";
    char* char_coordenadaY = "coordenadaY";
    char* char_coordenadaZ = "coordenadaZ";
    char* char_fecha = "fecha";
    char* char_idaceleracion = "idaceleracion";
    char* char_idprueba = "idprueba";

    //Creating a json string
    json_object *jstringFecha = json_object_new_string(fecha);

    //Creating json integers
    json_object *jint_idaceleracion = json_object_new_int(idaceleracion);
    json_object *jint_idprueba = json_object_new_int(idprueba);

    //Creating json doubles
    json_object *jdouble_coordenadaX = json_object_new_double(coordenadaX);
    json_object *jdouble_coordenadaY = json_object_new_double(coordenadaY);
    json_object *jdouble_coordenadaZ = json_object_new_double(coordenadaZ);

    //Form the json object
    //Each of these is like a key value pair
    json_object_object_add(jobj, char_coordenadaX, jdouble_coordenadaX);
    json_object_object_add(jobj, char_coordenadaY, jdouble_coordenadaY);
    json_object_object_add(jobj, char_coordenadaZ, jdouble_coordenadaZ);
    json_object_object_add(jobj, char_fecha, jstringFecha);
    json_object_object_add(jobj, char_idaceleracion, jint_idaceleracion);
    json_object_object_add(jobj, char_idprueba, jint_idprueba);

    json_object_array_add(jarray, jobj);
}

void setConfigText(Configuration* conf, char* key, const char* value) {

    if (strcmp(key, "device") == 0) {
        strcpy(conf->device, value);
    }
}

/**
 *  This method is use to parse a JSON object key-value pair
 * @param conf
 * @param key
 * @param value
 */
void setConfigInteger(Configuration* conf, char* key, int value) {

    if (strcmp(key, "idconfiguracion") == 0) {
        conf->idconfiguracion = value;
    } else if (strcmp(key, "periodo") == 0) {
        conf->periodo = value;
    } else if (strcmp(key, "idprueba") == 0) {
        conf->idprueba = value;
    } else if (strcmp(key, "sensor") == 0) {
        conf->sensor = value;
    } else if (strcmp(key, "estadistica") == 0) {
        conf->statistic = value;
    }
}

/**
 * This method is use to parse a JSON object
 * @param conf
 * @param jobj
 */
void json_parse(Configuration* conf, json_object * jobj) {

    enum json_type type;

    json_object_object_foreach(jobj, key, val) {
        type = json_object_get_type(val);
        switch (type) {
            case json_type_null:
                printf("json_type_nulln");
                break;
            case json_type_boolean:
                break;
            case json_type_double:
                break;
            case json_type_int:
                setConfigInteger(conf, key, json_object_get_int(val));
                break;
            case json_type_string:
                setConfigText(conf, key, json_object_get_string(val));
                break;
        }
    }
}

/**
 * This method is used to obtain the specified resource from the web service
 * based on the provided URL
 * @param msg
 * @param url
 * @return 
 */
int requestConfigurationJSON(Message* msg, char* url) {

    CURL *ch; // curl handle
    CURLcode rcode; // curl result code

    struct curl_fetch_st curl_fetch; // curl fetch struct
    struct curl_fetch_st *cf = &curl_fetch; // pointer to fetch struct
    struct curl_slist *headers = NULL; // http headers to send with request

    //Memory allocation
    char* msg_aux = (char *) malloc(sizeof (char));

    // init curl handle
    if ((ch = curl_easy_init()) == NULL) {
        // log error
        fprintf(stderr, "ERROR: Failed to create curl handle in fetch_session");
        // return error
        msg->code = ECCHFS;
        msg->message = msg_aux;
        return;
    }

    // set content type
    headers = curl_slist_append(headers, "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
    headers = curl_slist_append(headers, "Content-Type: application/json");

    // set curl options
    curl_easy_setopt(ch, CURLOPT_CUSTOMREQUEST, "GET");

    // fetch page and capture return code
    rcode = curl_fetch_url(ch, url, cf);

    // cleanup curl handle
    curl_easy_cleanup(ch);

    // check return code
    if (rcode != CURLE_OK || cf->size < 1) {
        // log error
        fprintf(stderr, "ERROR: Failed to fetch url (%s) - curl said: %s", url, curl_easy_strerror(rcode));
        // return error
        msg->code = EFFURL;
        msg->message = msg_aux;
        return;
    }

    // check payload
    if (cf->payload != NULL) {
        msg->message = cf->payload;
        msg->code = SUCC;
        free(msg_aux);
    } else {
        // error
        fprintf(stderr, "ERROR: Failed to populate payload");
        // free payload
        free(cf->payload);
        // return
        msg->code = EFPP;
        msg->message = msg_aux;
        return;
    }
}

/**
 * This method is used to update an specific configuration on the database,
 * the URL must provide the ID of the configuration that will be updated
 * @param msg
 * @param configuration
 * @return 
 */
int updateWebServiceConf(Message* msg, int configuration) {

    char *url = malloc(sizeof (char) * 128); // buffer for 128 chars
    configIdsetup(url, configuration);

    updateConfiguration(msg, url);

    if (msg->code == SUCC) {

        if (strcmp(msg->message, "failure") == 0) {
            printf("%s\n", "    The configuration can't be updated in the DB");
            Log("The configuration was not be updated in the DB", ECNU);
        } else if (strcmp(msg->message, "succesfull") == 0) {
            printf("%s\n", "    The configuration was updated in the DB"); //hacer algo si falla
            Log("The configuration was updated in the DB", 0);
        } else {
            printf("%s\n", "     Information return as an error");
            Log("Information from webservice return as an error", EIRAE);
        }

    } else {
        printf("Error in accessing the webservice error code: %d \n", msg->code);
        Log("Error in accessing the webservice", msg->code);
    }

    free(url); // Free the memory allocated by the URL stream 
    free(msg->message); // Free the memory allocated by the Message stream 

}

/**
 * This method is used to update an specified resource on the web service,
 * based on the provided URL
 * @param msg
 * @param url
 * @return 
 */
int updateConfiguration(Message* msg, char* url) {

    CURL *ch; // curl handle
    CURLcode rcode; // curl result code

    enum json_tokener_error jerr = json_tokener_success; // json parse error

    struct curl_fetch_st curl_fetch; // curl fetch struct
    struct curl_fetch_st *cf = &curl_fetch; // pointer to fetch struct

    //Memory allocation
    char* msg_aux = (char *) malloc(sizeof (char));

    // init curl handle
    if ((ch = curl_easy_init()) == NULL) {
        // log error
        fprintf(stderr, "ERROR: Failed to create curl handle in fetch_session");
        // return error
        msg->code = ECCHFS;
        msg->message = msg_aux;
        return;
    }

    // set curl options
    curl_easy_setopt(ch, CURLOPT_CUSTOMREQUEST, "GET");

    // fetch page and capture return code
    rcode = curl_fetch_url(ch, url, cf);

    // cleanup curl handle
    curl_easy_cleanup(ch);


    // check return code
    if (rcode != CURLE_OK || cf->size < 1) {
        // log error
        fprintf(stderr, "ERROR: Failed to fetch url (%s) - curl said: %s", url,
                curl_easy_strerror(rcode));
        // return error
        msg->code = EFFURL;
        msg->message = msg_aux;
        return;
    }

    // check payload
    if (cf->payload != NULL) {
        msg->message = cf->payload;
        msg->code = SUCC;
        free(msg_aux);
    } else {
        // error
        fprintf(stderr, "ERROR: Failed to populate payload");
        // free payload
        free(cf->payload);
        // return
        msg->code = EFPP;
        msg->message = msg_aux;
        return;
    }
}

/**
 * This method reads a configuration file and store the configurations into a 
 * structure called Parameters, the configuration file must always be in the
 * direction: /home/pi/Project_Ebridge.
 * If the the configuration file is not found the program can not be executed
 */
void getParameters() {

    config_t cfg;
    config_setting_t *setting;

    // Filepath
    char *config_file_name = "/home/pi/Project_Ebridge/parameters.conf";

    // Initialization
    config_init(&cfg);

    // Read the file. If there is an error, report it and exit
    if (!config_read_file(&cfg, config_file_name)) {

        printf("%s:%d - %s\n", config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
        printf("%s\n", "Configuration File Not Found");
        Log("Configuration file not found", ECFNF);
        config_destroy(&cfg);
        exit(0);
    }

    // Constant chars for storing strings
    const char *str1, *str2, *str3, *str4, *str5, *str6, *str7, *ip;

    // Integers for storing numbers
    int tmp1, tmp2, tmp3, tmp4, tmp5, tmp14, tmp15, tmp16;

    // Integers for storing floating point numbers
    double tmp6, tmp7, tmp8, tmp9, tmp10, tmp11, tmp12, tmp13;

    // Read the parameter group
    setting = config_lookup(&cfg, "parameter");
    if (setting != NULL) {

        config_setting_lookup_string(setting, "IP", &ip);
        config_setting_lookup_string(setting, "DEVICE_ID", &str1);
        config_setting_lookup_string(setting, "SERVICE_CONF", &str2);
        config_setting_lookup_string(setting, "SERVICE_ACCEL", &str3);
        config_setting_lookup_string(setting, "SERVICE_STD", &str4);
        config_setting_lookup_string(setting, "SERVICE_CONF_UP", &str5);
        config_setting_lookup_string(setting, "SERVICE_ALARM", &str6);
        config_setting_lookup_string(setting, "DEVICE_ID", &str7);

        config_setting_lookup_int(setting, "GPIOA", &tmp1);
        config_setting_lookup_int(setting, "GPIOB", &tmp2);
        config_setting_lookup_int(setting, "GPIOC", &tmp3);
        config_setting_lookup_int(setting, "WAIT_TIME", &tmp4);
        config_setting_lookup_int(setting, "BLOCK", &tmp5);

        config_setting_lookup_float(setting, "SCALE", &tmp6);
        config_setting_lookup_float(setting, "SENSIBILITY", &tmp7);

        config_setting_lookup_float(setting, "ALERT_MIN_X", &tmp8);
        config_setting_lookup_float(setting, "ALERT_MAX_X", &tmp9);
        config_setting_lookup_float(setting, "ALERT_MIN_Y", &tmp10);
        config_setting_lookup_float(setting, "ALERT_MAX_Y", &tmp11);
        config_setting_lookup_float(setting, "ALERT_MIN_Z", &tmp12);
        config_setting_lookup_float(setting, "ALERT_MAX_Z", &tmp13);

        config_setting_lookup_int(setting, "TOTAL_SENSORS", &tmp14);
        config_setting_lookup_int(setting, "FILTER_TYPE", &tmp15);
        config_setting_lookup_int(setting, "CHECK_TIME", &tmp16);
    }

    // Copy the data from the variables to the structure
    strcpy(parameters.SERVICE_CONF, ip);
    strcpy(parameters.SERVICE_ACCEL, ip);
    strcpy(parameters.SERVICE_STD, ip);
    strcpy(parameters.SERVICE_CONF_UP, ip);
    strcpy(parameters.SERVICE_ALARM, ip);

    strcat(parameters.SERVICE_CONF, str2);
    strcat(parameters.SERVICE_CONF, str1);
    strcat(parameters.SERVICE_ACCEL, str3);
    strcat(parameters.SERVICE_STD, str4);
    strcat(parameters.SERVICE_CONF_UP, str5);
    strcat(parameters.SERVICE_ALARM, str6);

    strcpy(parameters.DEVICE_ID, str7);

    parameters.GPIOA = tmp1;
    parameters.GPIOB = tmp2;
    parameters.GPIOC = tmp3;
    parameters.WAIT_TIME = tmp4;
    parameters.BLOCK = tmp5;
    parameters.SCALE = tmp6;
    parameters.SENSIBILITY = tmp7;
    parameters.ALERT_MIN_X = tmp8;
    parameters.ALERT_MAX_X = tmp9;
    parameters.ALERT_MIN_Y = tmp10;
    parameters.ALERT_MAX_Y = tmp11;
    parameters.ALERT_MIN_Z = tmp12;
    parameters.ALERT_MAX_Z = tmp13;
    parameters.TOTAL_SENSORS = tmp14;
    parameters.CHECK_TIME = tmp16;
    parameters.AccelerationFactor = parameters.SENSIBILITY / parameters.SCALE;
    parameters.FILTER_TYPE = tmp15;

    // Destroy the config_setting_t object
    config_destroy(&cfg);
}

/**
 * Starts timer and resets the elapsed time
 */
void timerStart() {

    struct timeval tod;
    gettimeofday(&tod, NULL);
    startTime = (double) tod.tv_sec + ((double) tod.tv_usec * 1.0e-6);
}

/**
 * Stops the timer and returns the elapsed time
 * @return 
 */
double timerStop() {

    struct timeval tod;
    gettimeofday(&tod, NULL);
    return ((double) tod.tv_sec + ((double) tod.tv_usec * 1.0e-6)) -startTime;
}

/**
 * This method returns the current date including the current hour, min and sec
 * @return String with Date
 */
char* get_time() {

    time_t t;
    char *buf;
    time(&t);
    buf = (char*) malloc(strlen(ctime(&t)) + 1);
    snprintf(buf, strlen(ctime(&t)), "%s ", ctime(&t));
    return buf;
}

/**
 * This method is use to store into the Log file an error message and an error code
 * @param error_message
 * @param code
 */
void Log(char* error_message, int code) {

    FILE* fp = fopen("log.txt", "a");
    char* time_now = get_time();
    fprintf(fp, "Time: %s ", time_now);

    fprintf(fp, "Code: %d ", code);
    fprintf(fp, "Error: %s ", error_message);
    fprintf(fp, "Device: %s ", parameters.DEVICE_ID);

    free(time_now);
    fputc('\n', fp);

    fclose(fp);
}
