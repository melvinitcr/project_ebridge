/* 
 * File:   ebridge_sensor.h
 * Author: Melvin Gutierrez V
 *
 * Created on May 1, 2016, 9:52 AM
 */
#ifndef EBRIDGE_SENSOR_H
#define EBRIDGE_SENSOR_H

#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>  
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <wiringPi.h>
#include <math.h>
#include <json/json.h>
#include <libconfig.h>
#include <signal.h>
#include <sys/time.h>
#include <pthread.h>

#define MPU6050_ADDRESS (0x68)
#define MPU6050_REG_PWR_MGMT_1 (0x6b)
#define MPU6050_REG_DATA_START (0x3b)
#define MPU6050_RA_SMPLRT_DIV (0x19)
#define MPU6050_RA_GYRO_CONFIG (0x1B)
#define MPU6050_RA_ACCEL_CONFIG (0x1C)
#define MPU6050_CONFIG (0x1A)

//SUCCESSFUL CODE
#define SUCC  0 // Successful at procedure 

//ERROR CODES
#define ECCHFS   1  // Failed to create curl handle in fetch_session.
#define EFFURL   2  // Failed to fetch url.
#define EFPP     3  // Failed to populate payload
#define EFPJSON  4  // Failed to parse json string
#define EFEBCURL -1  // Failed to expand buffer in curl_callback

#define ECFNF (100)  // Configuration file not found
#define ECNCS (101)  // Could not connect to the sensor
#define EJNNS (102) //The JSON data could not be saved in the DB
#define EIRAE (103) //Information from webservice return as an error
#define ECNU (104) //The configuration can't be updated in the DB

// Global variables
int SAMPLES;
int TESTNUMBER;
int SENSORNUMBER;
int fd;

/**
 * This structure is use to store error or successful codes and the responses 
 * when the program performs a WebService request
 */
typedef struct {
    int code;
    char *message;
} Message;

/**
 * This structure is use to store the information of a configuration when the
 * program performs a WebService request
 */
typedef struct {
    int idconfiguracion;
    char device[100];
    int sensor;
    int periodo;
    int idprueba;
    int statistic;
} Configuration;

/**
 * This structure is use to store the information read from the parameters file
 */
typedef struct {
    char DEVICE_ID[150];
    char SERVICE_CONF[150];
    char SERVICE_ACCEL[150];
    char SERVICE_STD[150];
    char SERVICE_CONF_UP[150];
    char SERVICE_ALARM[150];
    int GPIOA;
    int GPIOB;
    int GPIOC;
    int BLOCK;
    float SCALE;
    float SENSIBILITY;
    int WAIT_TIME;
    float ALERT_MIN_X;
    float ALERT_MAX_X;
    float ALERT_MIN_Y;
    float ALERT_MAX_Y;
    float ALERT_MIN_Z;
    float ALERT_MAX_Z;
    int TOTAL_SENSORS;
    int FILTER_TYPE;
    int CHECK_TIME;
    float AccelerationFactor;
} Parameters;

/**
 * This structure is use to store a coordinate that contains x, y & z values
 */
typedef struct {
    float x;
    float y;
    float z;
} Coordenate;

/**
 * This structure is use to store the information about statistical data,
 * obtained from reading the sensors data
 */
typedef struct {
    float average_x;
    float variance_x;
    float std_dev_x;
    float max_x;
    float min_x;

    float average_y;
    float variance_y;
    float std_dev_y;
    float max_y;
    float min_y;

    float average_z;
    float variance_z;
    float std_dev_z;
    float max_z;
    float min_z;
} Statistic;

/**
 * This structure is use to store the response and the response size from
 * a WebService request
 */
struct curl_fetch_st {
    char *payload;
    size_t size;
};

void start_Sensors();

void inspectData(int sensor);

void take_and_send_samples(Message*, char*);

void take_and_send_stadistics(Message*, char*);

void transform_into_JSON(Statistic*, json_object*, int, int, char*);

void transform_Alarm_into_JSON(json_object*, int, int, float, float, float, char*, int, char*, char*);

void sendWebServiceJSON(Message*, json_object*, char*);

void configIdsetup(char*, int);

void gpioWrite(int, int);

void selectSensor(int);

void getStdX(Coordenate*, int, Statistic*);

void getStdY(Coordenate*, int, Statistic*);

void getStdZ(Coordenate*, int, Statistic*);

void getStadistics(int, Statistic*);

int getWebServiceAprobal(Configuration*, Message*, char*);

int updateWebServiceConf(Message*, int);

void configureDevice(int);

int openConnection_to_Device();

int mymillis();

void checkRC(int, char*);

short getAccelX();

short getAccelY();

short getAccelZ();

void getSamples(json_object*, int);

void sendSamplesToFTP();

size_t curl_callback(void*, size_t, size_t, void*);

CURLcode curl_fetch_url(CURL*, const char*, struct curl_fetch_st*);

void addDataJSON(json_object*, float, float, float, int, int, char*);

int sendJSONData(Message*, json_object*, char*);

void setConfigText(Configuration*, char*, const char*);

void setConfigInteger(Configuration*, char*, int);

void json_parse(Configuration*, json_object*);

int requestConfigurationJSON(Message*, char*);

int updateConfiguration(Message*, char*);

void getParameters();

void timerStart();

double timerStop();

char* get_time();

void Log(char*, int);

//Global parameter structure
Parameters parameters;

//Global time parameter
double startTime;


#endif /* EBRIDGE_SENSOR_H */

