-- Table: aceleracion

-- DROP TABLE aceleracion;

CREATE TABLE aceleracion
(
  idaceleracion serial NOT NULL,
  idsensor serial NOT NULL,
  fecha timestamp(6) without time zone,
  coordenada_x real NOT NULL,
  coordenada_y real NOT NULL,
  coordenada_z real NOT NULL,
  idprueba integer,
  CONSTRAINT aceleracion_pkey PRIMARY KEY (idaceleracion),
  CONSTRAINT aceleracion_idsensor_fkey FOREIGN KEY (idsensor)
      REFERENCES sensor (idsensor) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE aceleracion
  OWNER TO postgres;


-- Table: alarma

-- DROP TABLE alarma;

CREATE TABLE alarma
(
  id_alarma serial NOT NULL,
  nivel integer NOT NULL,
  coordenada_x real NOT NULL,
  coordenada_y real NOT NULL,
  coordenada_z real NOT NULL,
  mensaje character varying(200) NOT NULL,
  numero_sensor integer NOT NULL,
  device text NOT NULL,
  fecha timestamp(6) without time zone NOT NULL,
  vista boolean NOT NULL,
  CONSTRAINT alarma_pkey PRIMARY KEY (id_alarma)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE alarma
  OWNER TO postgres;



-- Table: configuracion

-- DROP TABLE configuracion;

CREATE TABLE configuracion
(
  idconfiguracion serial NOT NULL,
  device text,
  periodo integer,
  realizado boolean,
  idprueba integer,
  sensor integer,
  estadistica integer,
  CONSTRAINT pk_config PRIMARY KEY (idconfiguracion)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE configuracion
  OWNER TO postgres;


-- Table: estadistica

-- DROP TABLE estadistica;

CREATE TABLE estadistica
(
  promedio_x real NOT NULL,
  promedio_y real NOT NULL,
  promedio_z real NOT NULL,
  varianza_x real NOT NULL,
  varianza_y real NOT NULL,
  varianza_z real NOT NULL,
  std_dev_x real NOT NULL,
  std_dev_y real NOT NULL,
  std_dev_z real NOT NULL,
  max_x real NOT NULL,
  max_y real NOT NULL,
  max_z real NOT NULL,
  min_x real NOT NULL,
  min_y real NOT NULL,
  min_z real NOT NULL,
  id_estadistica serial NOT NULL,
  idprueba integer,
  idsensor serial NOT NULL,
  fecha timestamp(6) without time zone,
  CONSTRAINT pk_estadistica PRIMARY KEY (id_estadistica),
  CONSTRAINT estadistica_idsensor_fkey FOREIGN KEY (idsensor)
      REFERENCES sensor (idsensor) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE estadistica
  OWNER TO postgres;


-- Table: sensor

-- DROP TABLE sensor;

CREATE TABLE sensor
(
  idsensor serial NOT NULL,
  fecha timestamp(6) without time zone NOT NULL,
  id_elemento_ligado integer NOT NULL,
  tabla_elemento_ligado character varying(30) NOT NULL,
  identificacion integer NOT NULL,
  factor_calibracion integer NOT NULL,
  ubicacion_x integer NOT NULL,
  ubicacion_y integer NOT NULL,
  ubicacion_z integer NOT NULL,
  activo boolean,
  idtipo integer,
  CONSTRAINT sensor_pkey PRIMARY KEY (idsensor),
  CONSTRAINT sensor_idtipo_fkey FOREIGN KEY (idtipo)
      REFERENCES tiposensor (idtipo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sensor
  OWNER TO postgres;


-- Table: tiposensor

-- DROP TABLE tiposensor;

CREATE TABLE tiposensor
(
  idtipo serial NOT NULL,
  tipo character varying(30) NOT NULL,
  descripcion character varying(100) NOT NULL,
  CONSTRAINT idtipo_pkey PRIMARY KEY (idtipo)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tiposensor
  OWNER TO postgres;
